# API COINSBIT/WS API COINSBIT
[![Coinsbit api](https://i.imgur.com/J6mDf8h.png)](https://coinsbit.io/)
# Authentication
```php
use GuzzleHttp\Client;

public function callApi()
{
    $apiKey = '';
    $apiSecret = '';
		$request = ''; // /api/v1/order/new
    $baseUrl = 'https://coinsbit.io';

    $data = [
        'request' => $request,
        'nonce' => (string)\Carbon\Carbon::now()->timestamp,
    ];

    $completeUrl = $baseUrl . $request;
    $dataJsonStr = json_encode($data, JSON_UNESCAPED_SLASHES);
    $payload = base64_encode($dataJsonStr);
    $signature = hash_hmac('sha512', $payload, $apiSecret);

    $client = new Client();
    try {
        $res = $client->post($completeUrl, [
            'headers' => [
                'Content-type' => 'application/json',
                'X-TXC-APIKEY' => $apiKey,
                'X-TXC-PAYLOAD' => $payload,
                'X-TXC-SIGNATURE' => $signature
            ],
            'body' => json_encode($data, JSON_UNESCAPED_SLASHES)
        ]);
    } catch (\Exception $e) {
        return response()->json(['error' => $e->getMessage()]);
    }

    return response()->json(['result' => json_decode($res->getBody()->getContents())]);
}
```
# Public API
### /api/v1/public/markets

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
response: {
    success: true,
    message: "",
    result: [
        {
            "name": "ETH_BTC",
            "moneyPrec": 8,
            "stock": "ETH",
            "money": "BTC",
            "stockPrec": 8,
            "feePrec": 4,
            "minAmount": "0.001"
        },
        {...},
    ]
}

response: {
    success: false,
    message: "Result is empty",
    result: []
}

response: {
    success: false,
    message: [
        [
            "//Error message...",
        ]
    ]
    result: []
}

```
</details>

### /api/v1/public/tickers

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
response: {
    success: true,
    message: "",
    result: {
        "ETH_BTC": {
            "at": 1533812472,
            "ticker": {
                "bid": "0.01",
                "ask": "0",
                "low": "0.01",
                "high": "0.01",
                "last": "0.01",
                "vol": "2"
            }
        },
        "BTC_USD": {
            "at": 1533812472,
            "ticker": {
                "bid": "20",
                "ask": "6644.99999973",
                "low": "20",
                "high": "6720",
                "last": "6650.02498321",
                "vol": "80.54157345"
            }
        },
        {
            ...
        }
    }
}

```
</details>


### /api/v1/public/ticker

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
request: {
    market: ETH_BTC //ETH_BTC, BTC_ETH ...etc
}

response: {
    success: true,
    message: "",
    result: {
        "volume": "0",
        "open": "2",
        "low": "2",
        "last": "2",
        "deal": "0",
        "high": "2"
    }
}

response: {
    success: false,
    message: "Result is empty",
    result: []
}

response: {
    success: false,
    message: [
        [
            "//Error message...",
        ]
    ]
    result: []
}
```
</details>

### /api/v1/public/book

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
market: "ETH_BTC" //ETH_BTC, BTC-ETH ...etc
side: "sell" //or buy
offset: 0 //optional; default value 0
limit: 100 //optional; default value = 50
}

response: {
    "success": true,
    "message": "",
    "result": {
        "offset": 0,
        "limit": 50,
        "total": 5,
        "orders": [{
                "id": 9389,
                "left": "1",
                "market": "ETH_BTC",
                "amount": "1",
                "type": 1,
                "price": "1",
                "ctime": 1533559966.19961,
                "side": 2,
                "dealFee": "0",
                "mtime": 1533559966.19961,
                "takerFee": "0.002",
                "makerFee": "0.002",
                "dealStock": "0",
                "dealMoney": "0"
            },
            {
                ...
            }
        ] // or empty array
    }
}

response: {
    success: false,
    message: "Result is empty",
    result: []
}

response: {
    success: false,
    message: [
        [
            "//Error message...",
        ]
    ]
    result: []
}
```
</details>



### /api/v1/public/history

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
request: {
    market: "ETH_BTC" //ETH_BTC, BTC_ETH ...etc
    lastId: 1
    limit: 100 //optional; default value = 50
}

response: {
    success: true,
    message: "",
    result: [{
            "id": 265,
            "time": 1533292963.202649,
            "price": "2",
            "type": "sell",
            "amount": "1"
        },
        {
            "id": 263,
            "time": 1533292880.088096,
            "price": "2",
            "type": "sell",
            "amount": "1"
        },
        {
            ...
        }
    ]
}

response: {
    success: false,
    message: "Result is empty",
    result: []
}

response: {
    success: false,
    message: [
        [
            "//Error message...",
        ]
    ]
    result: []
}
```
</details>

### /api/v1/public/history/result

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
request: {
    market: "ETH_BTC" //ETH_BTC, BTC_ETH ...etc
    since: 1
    limit: 100 //optional; default value = 50
}

response: [{
        "tid": 265,
        "date": 1533292963,
        "price": "2",
        "type": "sell",
        "amount": "1"
    },
    {
        "tid": 263,
        "time": 1533292880,
        "price": "2",
        "type": "sell",
        "amount": "1"
    },
    {
        ...
    }
]

response: {
    success: false,
    message: [
        [
            "//Error message...",
        ]
    ]
    result: []
}
```
</details>

### /api/v1/public/products

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
response: {
    success: true,
    message: "",
    result: [{
            "id": "ETH_BTC",
            "fromSymbol": "ETH",
            "toSymbol": "BTC"
        },
        {
            "id": "OCC_BTC",
            "fromSymbol": "OCC",
            "toSymbol": "BTC"
        },
        {
            ...
        }
    ]
}
```
</details>

### /api/v1/public/symbols

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
response: {
    success: true,
    message: "",
    result: [
        "ETH_BTC",
        "OCC_BTC",
        "OCC_ETH",
        "OCC_USD",
        "BTC_USD",
        "ETH_USD",
        ...
    ]
}
```
</details>

### /api/v1/public/depth/result

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
request: {
    market: "ETH_BTC" //ETH_BTC, BTC_ETH ...etc
    limit: 100 //optional; default value = 50
}

response: {
    "asks": [
        [
            "6322.8",
            "3.05500305"
        ],
        [
            "6323.9",
            "0.00825301"
        ],
        [...],
    ],
    "bids": [
        [
            "6322.8",
            "3.05500305"
        ],
        [
            "6323.9",
            "0.00825301"
        ],
        [...],
    ]
}
```
</details>

### api/v1/public/kline

<details>
<summary>For authorized (POST) or unauthorized (GET)</summary>

```javascript
request: {
    market: "BTC_USDT" //ETH_BTC, BTC_ETH ...etc
    start: 1601359005 //unixtime from
    end: 1601389005 //unixtime to
    interval: 60 //min interval 60
}

response: [{
        "time": 1601380800,
        "open": "10758.54",
        "close": "10762.13",
        "highest": "10767.87134838",
        "lowest": "10743.07002473",
        "volume": "290.58919799",
        "amount": "3125292.5472053327548651",
        "market": "BTC_USDT"
    },
    {
        "time": 1601384400,
        "open": "10762.13",
        "close": "10757.51",
        "highest": "10765.46968817",
        "lowest": "10715.11133846",
        "volume": "177.92246995",
        "amount": "1912137.3003313741065315",
        "market": "BTC_USDT"
    },
    {
        "time": 1601388000,
        "open": "10757.51",
        "close": "10758.23",
        "highest": "10788",
        "lowest": "10741.11",
        "volume": "71.68050299",
        "amount": "771598.2622076586289826",
        "market": "BTC_USDT"
    }
]
```
</details>

# Market API
### /api/v1/order/new

<details>
<summary>POST</summary>

```javascript
request: {
    "market": "ETH_BTC",
    "side": "sell",
    "amount": "0.1",
    "price": "0.1",
}

response: {
    "success": true,
    "message": "",
    "result": [
        "orderId": 25749,
        "market": "ETH_BTC",
        "price": "0.1",
        "side": "sell",
        "type": "limit",
        "timestamp": 1537535284.828868,
        "dealMoney": "0",
        "dealStock": "0",
        "amount": "0.1",
        "takerFee": "0.002",
        "makerFee": "0.002",
        "left": "0.1",
        "dealFee": "0"
    ]
}

response: {
    "success": false,
    "message": [
        [
            "Error message";
        ]
    ],
    "result": []
}
```
</details>

### /api/v1/order/cancel

<details>
<summary>POST</summary>

```javascript
request: {
    "market": "ETH_BTC",
    "orderId": 25749
}

response: {
    "success": true,
    "message": "",
    "result": [
        "orderId": 25749,
        "market": "ETH_BTC",
        "price": "0.1",
        "side": "sell",
        "type": "limit",
        "timestamp": 1537535284.828868,
        "dealMoney": "0",
        "dealStock": "0",
        "amount": "0.1",
        "takerFee": "0.002",
        "makerFee": "0.002",
        "left": "0.1",
        "dealFee": "0"
    ]
}

response: {
    "success": false,
    "message": [
        [
            "Error message";
        ]
    ],
    "result": []
}
```
</details>

### /api/v1/orders

<details>
<summary>POST</summary>

```javascript
request: {
    "market": "ETH_BTC",
    "offset": 10, //optional; default value 0
    "limit": 100 //optional; default value 50
}

response: {
    {
        "success": true,
        "message": "",
        "result": {
            "limit": 100,
            "offset": 10,
            "total": 17,
            "result": [{
                    "id": 9472,
                    "left": "1",
                    "market": "ETH_BTC",
                    "amount": "1",
                    "type": "limit" | "market",
                    "price": "0.01",
                    "timestamp": 1533561772.211871,
                    "side": "sell" | "buy",
                    "dealFee": "0",
                    "takerFee": "0",
                    "makerFee": "0",
                    "dealStock": "0",
                    "dealMoney": "0"
                },
                {
                    ...
                },
            ]
        }
    }

    response: {
        "success": false,
        "message": [
            [
                "Error message";
            ]
        ],
        "result": []
    }
}
```
</details>

# Account API

### /api/v1/account/balances

<details>
<summary>POST</summary>

```javascript
response: {
    {
        "success": true,
        "message": "",
        "result": {
            "ATB": {
                "available": "0",
                "freeze": "0"
            },
            "USD": {
                "available": "8990",
                "freeze": "0"
            },
            {
                ...
            },
        }
    }
}
```
</details>

### /api/v1/account/balance

<details>
<summary>POST</summary>

```javascript
request: {
    "currency": "ETH"
} //JSON

response: {
    {
        "success": true,
        "message": "",
        "result": {
            "available": "8990",
            "freeze": "0"
        }
    }

    {
        "success": false,
        "message": [
            [
                "Currency not found"
            ]
        ],
        "result": []
    }
}
```
</details>

### /api/v1/account/order

<details>
<summary>POST</summary>

```javascript
request: {
    "orderId": 1234,
    "offset": 10, //optional; default value 0
    "limit": 100 // optional; default value 50
} //JSON

response: {
    {
        "success": true,
        "message": "",
        "result": {
            "offset": 0,
            "limit": 50,
            "records": {
                "time": 1533310924.935978,
                "fee": "0",
                "price": "80.22761599",
                "amount": "2.12687945",
                "id": 548,
                "dealOrderId": 1237,
                "role": 1,
                "deal": "170.6344677716224055"
            }
        }
    }


    {
        "success": false,
        "message": [
            [
                "Error message"
            ]
        ],
        "result": []
    }
}
```
</details>

### /api/v1/account/trades

<details>
<summary>POST</summary>

```javascript
request: {
    "orderId": 1234,
    "offset": 10, //optional; default value 0
    "limit": 100 // optional; default value 50
} //JSON

response: {
    {
        "success": true,
        "message": "",
        "result": {
            "offset": 0,
            "limit": 50,
            "records": [{
                    "time": 1533310924.935978,
                    "fee": "0",
                    "price": "80.22761599",
                    "amount": "2.12687945",
                    "id": 548,
                    "dealOrderId": 1237,
                    "role": 1,
                    "deal": "170.6344677716224055"
                },
                {
                    ...
                }
            ]
        }
    }


    {
        "success": false,
        "message": [
            [
                "Error message"
            ]
        ],
        "result": []
    }
}
```
</details>

### /api/v1/account/order_history

<details>
<summary>POST</summary>

```javascript
request: {
    "offset": 10, //optional; default value 0
    "limit": 100 // optional; default value 50
}

response: {
        {
            "success": true,
            "message": "",
            "result": {
                "ETH_BTC": [{
                        "amount": "1",
                        "price": "0.01",
                        "type": "limit" | "market",
                        "id": 9740,
                        "side": "sell" | "buy",
                        "ctime": 1533568890.583023,
                        "takerFee": "0.002",
                        "ftime": 1533630652.62185,
                        "market": "ETH_BTC",
                        "makerFee": "0.002",
                        "dealFee": "0.002",
                        "dealStock": "1",
                        "dealMoney": "0.01",
                        "marketName": "ETH_BTC"
                    },
                    {
                        ...
                    }
                ],
                "ATB_USD": [{
                        "amount": "0.3",
                        "price": "0.06296168",
                        "type": "limit" | "market",
                        "id": 11669,
                        "source": "",
                        "side": "sell" | "buy",
                        "ctime": 1533626329.696647,
                        "takerFee": "0.002",
                        "ftime": 1533626329.696659,
                        "market": "ATB_USD",
                        "makerFee": "0.002",
                        "dealFee": "0.000037777008",
                        "dealStock": "0.3",
                        "dealMoney": "0.018888504",
                        "marketName": "ATB_USD"
                    },
                    {
                        ...
                    }
                ]
            }
        }
```
</details>

### /api/v1/account/order_history_list

<details>
<summary>POST</summary>

```javascript
request: {
    "offset": 10, //optional; default value 0
    "limit": 100 // optional; default value 50
}

response: {
        {
            "success": true,
            "message": "",
            "result": {
                "records": [{
                        "amount": "1",
                        "price": "0.01",
                        "type": "limit" | "market",
                        "id": 9740,
                        "side": "sell" | "buy",
                        "ctime": 1533568890.583023,
                        "takerFee": "0.002",
                        "ftime": 1533630652.62185,
                        "market": "ETH_BTC",
                        "makerFee": "0.002",
                        "dealFee": "0.002",
                        "dealStock": "1",
                        "dealMoney": "0.01",
                        "marketName": "ETH_BTC"
                    },
                    {
                        ...
                    }
                ]
            }
        }
```
</details>

# WS API COINSBIT

## API Protocol

The API is based on JSON RPC of Websocket protocol. Repeated subscription will be cancelled for the same data type.

**Request**

endpoint `wss://coinsbit.io/trade_ws`

- method: method，String
- params: parameters，Array
- id: Request id, Integer

**Response**

- result: Json object，null for failure
- error: Json object，null for success, non-null for failure
- id: Request id, Integer
1. code: error code
2. message: error message

**Notify**

- method: method，String
- params: parameters，Array
- id: Null

## Methods

**PING**

- method: server.ping
- params: None
- result: "pong"
- example: `{"method":"server.ping","params":[],"id":1000}`
- response: `{"error": null, "result": "pong", "id": 1000}`

**SYSTEM TIME**

- method: server.time
- params: none
- result: timestamp，Integer
- example: `{"method":"server.ping","params":[],"id":0}`
- response: `{"error": null, "result": 1493285895, "id": 1000}`

**MARKET SUBSCRIPTION**

- method: kline.subscribe
- params: market, interval
- example: `{"method":"kline.subscribe","params":["ETH_BTC",3600],"id":1000}`
- response: `{"method": "kline.update", "params": [[1568548260, "0.018302", "0.018302", "0.018302", "0.018302", "500", "15", "ETH_BTC"]], "id": null}`
- response params: `[ [
1568548260, time
"0.018302", open
"0.018302", close
"0.018302", highest
"0.018302", lowest
"500", volume
"15", amount
"ETH_BTC" market name ] ... ]`

**CANCEL SUBSCRIPTION**

- method: kline.unsubscribe
- params: none
- example: `{"method":"kline.unsubscribe","params":[],"id":1000}`

**LATEST PRICE SUBSCRIPTION**

- method: price.subscribe
- params: market list
- example: `{"method":"price.subscribe","params":["ETH_BTC","BTC_USD"],"id":111}`
- response: `{"method":"price.update","params":["ETH_BTC","0.01860351"],"id":null}`
- response params: `[
"ETH_BTC", market
"0.01860351", price ]`

**CANCEL SUBSCRIPTION**

- method: price.unsubscribe
- params: none
- example: `{"method":"price.unsubscribe","params":[],"id":1000}`

**ACQUIRE MARKET STATUS**

- method: state.query
- params: market, period(cycle period，Integer, e.g. 86400 for last 24 hours)
- example: `{"method":"state.query","params":["ETH_BTC",86400],"id":111}`
- response: `{"result": {"period": 86400, "volume": "192238.62392908", "last": "0.018295", "open": "0.017526", "low": "0.0174", "close": "0.018295", "high": "0.02", "deal": "3479.25570915213257"}, "error": null, "id": 111}`

**MARKET 24H STATUS SUBSCRIPTION**

- method: state.subscribe
- params: market list
- example: `{"method":"state.subscribe","params":["ETH_BTC","BTC_USD"],"id":111}`
- response: `{"method": "state.update", "params": ["BTC_USD", {"period": 86400, "volume": "6790.57915338", "high": "11000", "last": "10396.62814071", "low": "10291.07", "open": "10391.60804021", "close": "10396.62814071", "deal": "70615366.739616700295163"}], "id": null}`

**CANCEL SUBSCRIPTION**

- method: state.unsubscribe
- params: none
- example: `{"method":"state.unsubscribe","params":[],"id":111}`

**LATEST ORDER LIST SUBSCRIPTION**

- method: deals.subscribe
- params: market list
- example: `{"method":"deals.subscribe","params":["ETH_BTC","BTC_USD"],"id":111}`
- response: `{"method": "deals.update", "params": ["BTC_USD", [{"type": "sell", "time": 1568556382.1329091, "id": 5478754, "amount": "4.9193309", "price": "10365.40703518"}]], "id": null}`

**CANCEL SUBSCRIPTION**

- method: deals.unsubscribe
- params: none
- example: `{"method":"deals.unsubscribe","params":[],"id":111}`

**DEPTH SUBSCRIPTION**

- method: depth.subscribe
- params: market, limit, interval
- example: `{"method":"depth.subscribe","params":["ETH_BTC",1,"0"],"id":111}`
- response: `{"method": "depth.update", "params": [true, {"asks": [["0.018519", "120.6"]], "bids": [["0.01806", "90.31637262"]]}, "ETH_BTC"], "id": null}`

**CANCEL SUBSCRIPTION**

- method: depth.unsubscribe
- params: none
- example: `{"method":"depth.unsubscribe","params":[],"id":111}`
